# README #



### Branches ###

* Master -> stable version of decoupled frontend
* Develop -> development version of frontend
* WordPress -> WordPress installation used to serve content



### Demo Links ###

Demo site:

http://decoupled.eq.vc/


WordPress API output:

* Homepage: http://wordpress.eq.vc/
* Blog/posts page: http://wordpress.eq.vc/blog/
* Search page: http://wordpress.eq.vc/?s=lorem


### Custom WordPress API and Plugins ###

**Plugins**

* wp-change-theme-based on-subdomain: used to change theme based on subdomain used to access site
* wp-post-content-sections: manages "content sections", ie: banner, content, secondary and content "blocks" assigned to each section

**API**

* API is implemented as a WordPress theme, and stored in wp-content/themes/api



### Q&A ###

**Why build the API as a WP theme and not a plugin?**

I wanted to try something different. In a number of ways I'm glad I opted to build it as a theme. The process for changing the structure of the JSON is the same as modifying any-old WP theme template, so developers that are less familiar with hooks, actions and filters can still makes the changes they need with minimal learning curve. It also gives the ability to easily create different JSON "templates" for different kinds of data by using the amazing WP template hierarchy.


**Why not just use the API plugin that's being merged to core?**

The API plugin, in my opinion has some shortcomings that are inherent in a stock WordPress installation. Pages on a website, outside of a blog, are no longer simply a title and block of content. To handle this in WordPress, the result is often a flood of custom fields using something such as the fantastic plugins Pods or Advanced Custom Fields. Issue is, all of this gets returned in 'post_meta', which isn't very helpful or universal. The other method is to use a page builder, which end up sending a post_content field filled with HTML and markup--which, if you wanted to customize this layout on a decoupled frontend, you'd have to parse it using DOM_Document or similar. I feel that is inefficient and unnecessary.

We're separating concerns: markup, style, functionality, and now, **content**. You don't have markup in your backend code, so why would you want style or page structure markup in your content? The idea is to allow the developer to set the structure of the JSON and be able to adjust it on a per-project basis based on its unique needs. This is not something that the plugin allows.

In addition, the goal is for the API in this repo to fully support WordPress plugins, such as Gravity Forms, which the official API does not.