<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>WordPress | Just another WordPress site</title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="http://wordpress.eq.vc/xmlrpc.php">
	<!--[if lt IE 9]>
	<script src="http://wordpress.eq.vc/wp-content/themes/four-years/js/html5shiv.js"></script>
	<![endif]-->
	<meta name='robots' content='noindex,follow' />
	<link rel="alternate" type="application/rss+xml" title="Wordpress &raquo; Feed" href="http://wordpress.eq.vc/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Wordpress &raquo; Comments Feed" href="http://wordpress.eq.vc/comments/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Wordpress &raquo; Home Comments Feed" href="http://wordpress.eq.vc/home/feed/" />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://wordpress.eq.vc/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://wordpress.eq.vc/wp-includes/wlwmanifest.xml" /> 
	
	<meta name="generator" content="WordPress 4.1.1" />
    <meta name="generator" content="Thelia V2">
    <meta name="generator" content="x2engine">
    <link rel='canonical' href='http://decoupled.eq.vc/' />
	<link rel='shortlink' href='http://decoupled.eq.vc/' />

	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!--
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
-->
</head>

<body ng-app="wpDecoupled" ng-controller="customersCtrl">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">WordPress</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
			<li><a href="/blog/">Blog</a></li>
			<li><a href="/test/">test</a></li>
			<li><a href="/sample-page/">Sample Page</a></li>
<!--			<li><a href="" ng-click="href('/non-existent-page/')">Non-existent Page</a></li>
			<li><a href="" ng-click="href('/my-page/')">My Page</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

<div id="page"></div>

<?php include(__DIR__.'/footer.php');?>

<script src= "/bower_components/angular/angular.min.js"></script>
<script src= "/bower_components/jquery/dist/jquery.min.js"></script>
<!--
<script src= "/js/jquery.core.min.js"></script>
-->
<script src= "/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src= "/js/swig.min.js"></script>
<script src="/nanoajax/nanoajax.min.js"></script>
<script src="/js/app.js"></script>

</body>
</html>
