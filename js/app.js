function Swig_Templates() {
	this.a = nanoajax.ajax;
	this.templates = {};

	this.init = function() {
		window.addEvent = function(elem, type, callback) {
			var evt = function(e) {
					e = e || window.event;
					return callback.call(elem, e);
				},
				cb = function(e) {
					return evt(e);
				};
			if (elem.addEventListener) {
				elem.addEventListener(type, cb, false);
			} else if (elem.attachEvent) {
				elem.attachEvent("on" + type, cb);
			}
			return elem;
		};
		window.findParent = function(child, filter, root) {
			do {
				if (filter(child)) return child;
				if (root && child == root) return false;
			} while (child = child.parentNode);
			return false;
		};
		window.hasClass = function(elem, cls) {
			if (!('className' in elem)) return;
			return !!elem.className.match(new RegExp("\\b" + cls + "\\b"));
		};
		window.isElem = function(elem, tag) {
			return !!(elem.tagName.toLowerCase() == tag);
		}
		window.scrollToTop = function() {
			window.scrollTo(document.documentElement, 0, 250);
		}

		window.scrollTo = function(element, to, duration) {
			if (duration < 0) return;
			var difference = to - element.scrollTop;
			var perTick = difference / duration * 10;

			setTimeout(function() {
				element.scrollTop = element.scrollTop + perTick;
				if (element.scrollTop == to) return;
				scrollTo(element, to, duration - 10);
			}, 10);
		}
	}

	this.load = function(tpl, callback) {
		var t = this;

		if (!t.templates[t]) {
			var loader = new this.loader(tpl, t, callback);
		}
	}

	this.reload = function(tpl) {
		delete this.templates[tpl];

		this.load(tpl);
	}

	this.save = function(tpl, content, callback) {
		this.templates[tpl] = content;
		if(callback){
		    callback(tpl);
		}
	}
	
	this.not_found = function(code, callback){
	    callback('index');
	}

	this.render = function(filename, data, tpl, dest) {
	    var output = '';
	    var t = this;
	    
		if (!tpl) tpl = 'index';
		if (!dest) dest = '#page';

		if (this.templates[data.page.post_name]) {
			tpl = data.page.post_name;
			
     		output = swig.render(t.templates[tpl], {
     			filename: filename,
     			locals: data
     		});
    
    		document.querySelector(dest).innerHTML = output;
		}
		else{
		    this.load(data.page.post_name,function(tpl){
		        if(!t.templates[data.page.post_name]){
		            tpl = 'index';
		        }
		        
         		output = swig.render(t.templates[tpl], {
         			filename: filename,
         			locals: data
         		});
        
        		document.querySelector(dest).innerHTML = output;
		    });
		}
	}

	this.loader = function(tpl, obj, callback) {
		this.run = function() {
		    var t = this;
			var url = "/templates/" + tpl + '.twig';
			obj.a(url, t.return);
		}

		this.return = function(code, response) {
			if (code == 200) {
				obj.save(tpl, response, callback);
			}
			else{
			    obj.not_found(code, callback);
			}
		}

		this.run();
	}

	this.init();
}

var app = angular.module('wpDecoupled', []);
// var content_url = 'http://wordpress.eq.vc';
var content_url = 'http://decoupled.eq.vc/mid.php?path='
var host_url = window.location.protocol + '//' + window.location.host;
var content_cache = [];

app.config(function($sceProvider) {
	$sceProvider.enabled(false);
});

app.controller('customersCtrl', function($scope, $http) {
	$scope.init = function() {
		$scope.tpl = new Swig_Templates();
		$scope.bind();
 		$scope.tpl.load('home');
 		$scope.tpl.load('index');
 		$scope.tpl.load('blog');
 		$scope.href();
 		
 		$scope.cachetimeout = null;
 		$scope.cachetimeoutdelay = 25;
	}

	$scope.bind = function() {
		window.addEvent(document.body, "click", function(e) {
			e.preventDefault();
/*			var s = window.findParent(e.srcElement || e.target, function(elm) {
				return window.isElem(elm, "a");
			}, this);
			if (s) {
				e.preventDefault();
				var href = s.href.replace(host_url, '');
				$scope.href(href);
			}*/
		});
		window.addEvent(document.body, "mousedown", function(e) {
			var s = window.findParent(e.srcElement || e.target, function(elm) {
				return window.isElem(elm, "a");
			}, this);
			if (s) {
				e.preventDefault();
				var href = s.href.replace(host_url, '');
				$scope.href(href);
			}
		});
		window.addEvent(document.body, "mouseover", function(e) {
			var s = window.findParent(e.srcElement || e.target, function(elm) {
				return window.isElem(elm, "a");
			}, this);
			if (s) {
				var href = s.href.replace(host_url, '');
				$scope.cachetimeout = window.setTimeout(function(){
					$scope.precache(href);
				}, $scope.cachetimeoutdelay)
			}
		});
		window.addEvent(document.body, "mousemove", function(e) {
			var s = window.findParent(e.srcElement || e.target, function(elm) {
				return window.isElem(elm, "a");
			}, this);
			if (s) {
				window.clearTimeout($scope.cachetimeout);
				
				var href = s.href.replace(host_url, '');
				$scope.cachetimeout = window.setTimeout(function(){
					$scope.precache(href);
				}, $scope.cachetimeoutdelay)
			}
		});
		window.addEvent(document.body, "mouseout", function(e) {
			var s = window.findParent(e.srcElement || e.target, function(elm) {
				return window.isElem(elm, "a");
			}, this);
			if (s) {
				window.clearTimeout($scope.cachetimeout);
			}
		});
	}

	$scope.href = function(href) {
		var push_state = true;

		if (!href) {
			href = document.location.href.replace(host_url, '');
			push_state = false;
		}

		//	    console.log(href);

		if (content_cache[href]) {
			$scope.render(href, content_cache[href]);
			history.pushState(href, '', href);
		} else {
			getUrl = $scope.url(href);

			$http.get(getUrl)
				.success(function(response) {
					content_cache[href] = response;

					$scope.render(href, response);

					if (push_state) {
						history.pushState(href, '', href);
					} else {
						history.replaceState(href, '', href);
					}
				});
		}
	}
	
	$scope.precache = function(href){
		var push_state = true;

		if (!href) {
			href = document.location.href.replace(host_url, '');
			push_state = false;
		}

		if (content_cache[href]) {
			
		} else {
			getUrl = $scope.url(href);

			$http.get(getUrl)
				.success(function(response) {
					content_cache[href] = response;

				});
		}
	}

	$scope.render = function(filename, data) {
		window.scrollToTop();
		$scope.tpl.render(filename, data);
	}

	$scope.url = function(href) {
		return content_url + href; // + ((0 > href.indexOf('?')) ? "?" : "&") + "api"
	}

	window.onpopstate = function(event) {
		// alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
		$scope.render(event.state, content_cache[event.state]);
	};
	$scope.init();
});