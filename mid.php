<?php
header('Content-Type: application/json');

forp_start();
class Data_Store extends SQLite3 {
	function __construct( $name, $dir ) {
		$this->open( $dir . $name );
	}
	
	function insert( $table, $data, $return_query = false ) {
		$index = "`".implode("`, `",array_keys($data))."`";
		$value = [];
		
		foreach( $data as $i => $v ){
			if(is_numeric($v)){
				$value[] = $v;
			}
			else{
				$value[] = "'".addslashes($v)."'";
			}
		}
		
		$value = implode(", ", $value);
		
		$insert = "INSERT INTO `$table` ($index) VALUES ($value)";
		
		return (false === $return_query) ? $this->exec($insert) : $insert;
	}
	
	function fetch( $result ){
		$return = [];
		$record = $result->fetchArray(SQLITE3_ASSOC);
		
		foreach($record as $i => $v){
			$return[$i] = (is_numeric($v)) ? $v : stripslashes($v);
			
/*			if($array = json_decode($return[$i])){
				$return[$i] = $array;
			} */
		}
		
		return $return;
	}
	
	function select( $table, $where ){
		$select = "SELECT * FROM wp_cache WHERE $where ORDER BY id DESC LIMIT 1";
		
		$result = $this->query($select);
		return $result;
	}
}

$db = new Data_Store('db_cache', __DIR__.'/');

/*
$create = "CREATE TABLE IF NOT EXISTS \"wp_cache\" (
  \"id\" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  \"data\" text NOT NULL,
  \"timestamp\" integer NOT NULL
);";

$db->exec($create);
*/
$path = $_GET['path'] ?: "/"; //"/blog/";
$cache_path = crc32($path);

if($result = $db->select('wp_cache', "path='$cache_path'")){
	$record = $db->fetch($result);
	if(!empty($record)){
		echo $record['data'];
		exit;
	}
}

$call = "http://wordpress.eq.vc".$path."?api";

$data = file_get_contents($call);
$timestamp = time();

// $insert = "INSERT INTO `wp_cache` (`path`, `data`, `timestamp`) VALUES ($path, \"".addslashes($data)."\", $now)";

$cache_record = [
	'path' => $cache_path,
	'data' => $data,
	'timestamp' => $timestamp
];

$insert = $db->insert('wp_cache', $cache_record, true);
$result = $db->exec($insert);

echo $data;

forp_end();
// forp_json();