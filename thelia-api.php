<?php
spl_autoload_register(
    function($className)
    {
        $className = str_replace("_", "\\", $className);
        $className = ltrim($className, '\\');
        $fileName = '';
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\'))
        {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

        require $fileName;
    }
);

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$token = '0F28A349BC0A80AF9DCCE1B75';
//$resource = 'products';
$url = 'http://thelia.eq.vc/api/products/?sign='.getSignature();
$method = 'GET';
$sign = getSignature();

$post_fields = ['sign' => $sign];


/*
//$request = Request::create( $url, 'GET' );
// $request->headers->set('Authorization', 'Token '.$token);
// $request->query->set('sign', getSignature());

$request = new Request();
$response = $request->create( 'http://thelia.eq.vc/api/'.$resource, 'GET' );

 $products = $request->request->get($resource);//($resource, null);

echo "<pre>";
var_dump($products);
echo "</pre>";

echo $request->getPathInfo();
*/

function getSignature($requestContent = '')
{
    $secureKey = pack('H*', file_get_contents(dirname(__FILE__).'/'."0F28A349BC0A80AF9DCCE1B75.key"));

    return hash_hmac('sha1', $requestContent, $secureKey);
}


$curl = curl_init();
curl_setopt_array($curl, [
    CURLOPT_CUSTOMREQUEST => $method,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $url.$resource
]);

curl_setopt($curl,CURLOPT_HTTPHEADER,[
    'Authorization: Token '.$token
]);




$response = curl_exec($curl);

echo "<pre>";
print_r(json_decode($response));
exit;
echo $response;
